//
//  AppDelegate.swift
//  SDKDemoApp
//
//  Created by Mihai Fischer on 14/05/2019.
//  Copyright © 2019 Mihai Fischer. All rights reserved.
//

import UIKit
import AppTrackingTransparency
import GoogleMobileAds

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        
        if #available(iOS 14, *) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                ATTrackingManager.requestTrackingAuthorization { _ in
                }
            }
        } else {
            // Fallback on earlier versions
        }
        
        return true
    }
}
