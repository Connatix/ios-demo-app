//
//  ElementsPlayerCell.swift
//  SDKDemoApp
//
//  Created by Duminica Octavian on 03.12.2020.
//  Copyright © 2020 Mihai Fischer. All rights reserved.
//

import UIKit
import ConnatixPlayerSDK

class ElementsPlayerCell: UITableViewCell {
    
    @IBOutlet private weak var containerView: UIView!
    private var connatixPlayer: ElementsPlayer!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupPlayer()
    }
    
    func setupPlayer() {
        
        /*
         Player init
         Optionally, you can also define ElementsSettings and ElementsCustomization objects here
         */
        let appSettings = AppSettings(appID: "12345",
                                      domainURL: "https://example.com",
                                      storeURL: "https://appstore.com/example",
                                      appCategories: ["news"],
                                      hasPrivacyPolicy: true,
                                      isPaid: false,
                                      appPageURL: "https://example.com/web-equivalent-article-page",
                                      useContentOnlyDomain: true)
        
        let elementsConfig = ElementsConfig(playerId: "a1c25364-9ad9-4636-a194-7886a24aedf7",
                                            customerId: "51a004ea-0018-4342-83c4-9a983a0ee702",
                                            appSettings: appSettings)
        
        connatixPlayer = ElementsPlayer(frame: .zero,
                                        config: elementsConfig,
                                        delegate: self)
        
        connatixPlayer.view.translatesAutoresizingMaskIntoConstraints = false
        
        // Add player single event listeners
        connatixPlayer.listenFor(.pause)
        connatixPlayer.listenFor(.play)
        connatixPlayer.listenFor(.volumeChanged)
        
        // Or listen to all events
        connatixPlayer.listenForAllEvents()
        
        // Start rendering the player
        connatixPlayer.start()
        
        // Add player to layout
        containerView.addSubview(connatixPlayer.view)
        NSLayoutConstraint.activate([
            connatixPlayer.view.topAnchor.constraint(equalTo: containerView.topAnchor),
            connatixPlayer.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            connatixPlayer.view.leftAnchor.constraint(equalTo: containerView.leftAnchor),
            connatixPlayer.view.rightAnchor.constraint(equalTo: containerView.rightAnchor)
        ])
    }
}

// MARK: - ConnatixPlayerDelegate
extension ElementsPlayerCell: ConnatixPlayerDelegate {
    
    // Player events
    func receivedEvent(_ event: PlayerEvent?) {
        if let event = event {
            print("🔥 Event type: \(event.type.rawValue), payload: \(event.payload ?? [:])")
        }
    }
}
