//
//  ElementsView.swift
//  SDKDemoApp
//
//  Created by Duminica Octavian on 06.08.2024.
//  Copyright © 2024 Mihai Fischer. All rights reserved.
//

import SwiftUI
import ConnatixPlayerSDK
import ConnatixPlayerSDKSwiftUI

private let config = ElementsConfig(playerId: "a1c25364-9ad9-4636-a194-7886a24aedf7",
                                    customerId: "51a004ea-0018-4342-83c4-9a983a0ee702",
                                    appSettings: AppSettings(appID: "12345",
                                                             domainURL: "https://example.com",
                                                             storeURL: "https://appstore.com/example",
                                                             appCategories: ["news"],
                                                             hasPrivacyPolicy: true,
                                                             isPaid: false,
                                                             appPageURL: "https://example.com/web-equivalent-article-page",
                                                             useContentOnlyDomain: true))

class ElementsViewModel: ObservableObject, ConnatixPlayerDelegate {
    
    func receivedEvent(_ event: PlayerEvent?) {
        if let event {
            print("Event type: \(event.type.rawValue), payload: \(event.payload ?? [:])")
        }
    }
}

struct ElementsView: View {
    
    @ObservedObject private var viewModel = ElementsViewModel()
    @State private var viewDidLoad = false
    
    private var elementsPlayer = ElementsPlayerView(config: config)
    
    var body: some View {
        ScrollView {
            VStack(spacing: 20) {
                elementsPlayer
                    .aspectRatio(16 / 9, contentMode: .fit)
                
                ForEach(0..<10, id: \.self) { _ in
                    Image("tableCellPlaceholder")
                }
            }
            .padding()
            .onAppear {
                if viewDidLoad == false {
                    viewDidLoad = true
                    elementsPlayer.setDelegate(viewModel)
                    elementsPlayer.listenForAllEvents()
                    elementsPlayer.start()
                }
             }
        }
    }
}
