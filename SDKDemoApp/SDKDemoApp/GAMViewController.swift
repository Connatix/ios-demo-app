//
//  GAMViewController.swift
//  SDKDemoApp
//
//  Created by Duminica Octavian on 01.05.2022.
//  Copyright © 2022 Mihai Fischer. All rights reserved.
//

import UIKit
import GoogleMobileAds

class GAMViewController: UIViewController {
    
    private let adUnitId = "/22034550994/1029384756"
    private let adSize = GADAdSizeFromCGSize(CGSize(width: 300, height: 250))
    
    @IBOutlet private weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadAd(adUnitId, adSize)
    }
    
    private func loadAd(_ adUnitId: String, _ adSize: GADAdSize) {
        
        let bannerView = GAMBannerView(adSize: adSize)
        view.addSubview(bannerView)
        
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addConstraints([
            bannerView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            bannerView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor)
        ])
        
        bannerView.adUnitID = adUnitId
        bannerView.rootViewController = self
        
        // Add values for the macros defined in the grab code.
        // This grab code can be found on the Connatix console.
        // The values below are examples, you should provide real ones instead
        let gamRequest = GAMRequest()
        gamRequest.customTargeting = [
            "appPageURL": "https://example.com/web-equivalent-article-page",
            "us_privacy_string": "",
            "gdpr_parsed_vendor_string": "",
            "gdpr_consent": ""
        ]
        bannerView.load(gamRequest)
    }
}
