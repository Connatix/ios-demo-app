//
//  MainViewController.swift
//  SDKDemoApp
//
//  Created by Mihai Fischer on 14/05/2019.
//  Copyright © 2019 Mihai Fischer. All rights reserved.
//

import UIKit
import SwiftUI

class MainViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction private func didTapElementsSwiftUIButton(_ sender: UIButton) {
        let elementsSwiftUIView = ElementsView()
        let hostingController = UIHostingController(rootView: elementsSwiftUIView)
        hostingController.title = "Elements SwiftUI"
        navigationController?.pushViewController(hostingController, animated: true)
    }
    
    @IBAction private func didTapPlayspaceSwiftUIButton(_ sender: UIButton) {
        let elementsSwiftUIView = PlayspaceView()
        let hostingController = UIHostingController(rootView: elementsSwiftUIView)
        hostingController.title = "Playspace SwiftUI"
        navigationController?.pushViewController(hostingController, animated: true)
    }
}
