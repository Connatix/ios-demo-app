//
//  ElementsView.swift
//  SDKDemoApp
//
//  Created by Duminica Octavian on 06.08.2024.
//  Copyright © 2024 Mihai Fischer. All rights reserved.
//

import SwiftUI
import ConnatixPlayerSDK
import ConnatixPlayerSDKSwiftUI

private let config = PlayspaceConfig(playerId: "01eadd01-38d3-4143-ac4a-22a37eb2c1e8",
                                     customerId: "51a004ea-0018-4342-83c4-9a983a0ee702",
                                     appSettings: AppSettings(appID: "12345",
                                                              domainURL: "https://example.com",
                                                              storeURL: "https://appstore.com/example",
                                                              appCategories: ["news"],
                                                              hasPrivacyPolicy: true,
                                                              isPaid: false,
                                                              appPageURL: "https://example.com/web-equivalent-article-page"))

class PlayspaceViewModel: ObservableObject, ConnatixPlayerDelegate {
    
    func receivedEvent(_ event: PlayerEvent?) {
        if let event {
            print("Event type: \(event.type.rawValue), payload: \(event.payload ?? [:])")
        }
    }
}

struct PlayspaceView: View {
    
    @ObservedObject private var viewModel = PlayspaceViewModel()
    @State private var viewDidLoad = false
    
    private var playspacePlayer = PlayspacePlayerView(config: config)
    
    var body: some View {
        ScrollView {
            VStack(spacing: 20) {
                playspacePlayer
                    .aspectRatio(4 / 3, contentMode: .fit)
                
                ForEach(0..<10, id: \.self) { _ in
                    Image("tableCellPlaceholder")
                }
            }
            .padding()
            .onAppear {
                if viewDidLoad == false {
                    viewDidLoad = true
                    playspacePlayer.setDelegate(viewModel)
                    playspacePlayer.listenForAllEvents()
                    playspacePlayer.start()
                }
             }
        }
    }
}
