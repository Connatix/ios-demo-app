//
//  PlayspaceViewController.swift
//  SDKDemoApp
//
//  Created by Mihai Fischer on 10/10/2019.
//  Copyright © 2019 Mihai Fischer. All rights reserved.
//

import UIKit
import ConnatixPlayerSDK

class PlayspaceViewController: UIViewController {

    @IBOutlet private weak var containerView: UIView!
    
    private var connatixPlayer: PlayspacePlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupPlayer()
    }
    
    private func setupPlayer() {

        /*
         Player init
         Optionally, you can also define PlayerSettings and Customization objects here
         */
        let appSettings = AppSettings(appID: "12345",
                                      domainURL: "https://example.com",
                                      storeURL: "https://appstore.com/example",
                                      appCategories: ["news"],
                                      hasPrivacyPolicy: true,
                                      isPaid: false,
                                      appPageURL: "https://example.com/web-equivalent-article-page")
        
        // Optional
        let customization = PlayspaceCustomization(logoUrl: nil,
                                                   orientation: .landscape)
        
        // Optional
        let playerSettings = PlayspaceSettings(playbackMode: .autoPlay,
                                               defaultSoundMode: .on,
                                               customization: customization,
                                               disableAdvertising: nil,
                                               advertising: nil,
                                               disableClickUrl: nil,
                                               disableCTARendering: nil,
                                               disableStoryProgressBarRendering: nil,
                                               disableTitleRendering: nil)
        
        let playspaceConfig = PlayspaceConfig(playerId: "01eadd01-38d3-4143-ac4a-22a37eb2c1e8",
                                              customerId: "51a004ea-0018-4342-83c4-9a983a0ee702",
                                              appSettings: appSettings,
                                              settings: playerSettings)
        
        connatixPlayer = PlayspacePlayer(frame: .zero,
                                         config: playspaceConfig,
                                         delegate: self)
        connatixPlayer.view.translatesAutoresizingMaskIntoConstraints = false
        
        // Add player single event listeners
        connatixPlayer.listenFor(.pause)
        connatixPlayer.listenFor(.play)
        connatixPlayer.listenFor(.volumeChanged)
        
        // Or listen to all events
        connatixPlayer.listenForAllEvents()
        
        // Start rendering the player
        connatixPlayer.start()
        
        // Add player to layout
        containerView.addSubview(connatixPlayer.view)
        
        NSLayoutConstraint.activate([
            connatixPlayer.view.topAnchor.constraint(equalTo: containerView.topAnchor),
            connatixPlayer.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            connatixPlayer.view.leftAnchor.constraint(equalTo: containerView.leftAnchor),
            connatixPlayer.view.rightAnchor.constraint(equalTo: containerView.rightAnchor)
        ])
    }
}

// MARK: - ConnatixPlayerDelegate
extension PlayspaceViewController: ConnatixPlayerDelegate {
    
    // Player events
    func receivedEvent(_ event: PlayerEvent?) {
        if let event = event {
            print("🔴 Event type: \(event.type.rawValue), payload: \(event.payload ?? [:])")
        }
    }
}
